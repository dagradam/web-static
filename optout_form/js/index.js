function validateForm() {
    var emailListInput = document.forms["requestForm"]["emailList"].value;
    if (emailListInput == "") {
    	$('#emailListErrorMsg').html("<font color=red>List cannot be blank!</font>").fadeIn('slow');
        $('#emailListErrorMsg').delay(3000).fadeOut('slow');
    	javascript_abort();
        return false;
    }
    var emailInput = document.forms["requestForm"]["email"].value;
    if (emailInput == "") {
    	$('#emailErrorMsg').html("<font color=red>Requestor email cannot be blank!</font>").fadeIn('slow');
        $('#emailErrorMsg').delay(3000).fadeOut('slow');
    	javascript_abort();
        return false;
    }
}

function javascript_abort()
{
   throw new Error('This is not an error. This is just to abort javascript');
}

function CallPageSync(url, data) {
  var response;
  $.ajax({
    method: "POST",
    url: url,
    data: data,
    contentType: "application/json; charset=utf-8",
    beforeSend: function beforeSend(request) {
      request.setRequestHeader("X-SDC-APPLICATION-ID", "microservice");
    },
    crossDomain: true,
    timeout: 1000,
    success: function success(data) {

      if (data.httpStatusCode = "200") {
        successMsg = "OK - Form submitted.\nYou will be notified with an email once completed";
      }

      $('#message').html(successMsg).fadeIn('slow');
      $('#message').delay(5000).fadeOut('slow');
      $('#requestForm')[0].reset();
      $('#requestId').val($.now());

    },
    error: function error(data) {

      errorMsg = "<font color=red>Error!Unable to connect to server.\nPlease retry later.\n" + data.error + "-" + data.errorMessage + "</font>";
      $('#message').html(errorMsg).fadeIn('slow');
      $('#message').delay(5000).fadeOut('slow');

    } });

  return response;
}


/**
   * Checks that an element has a non-empty `name` and `value` property.
   * @param  {Element} element  the element to check
   * @return {Bool}             true if the element is an input, false if not
   */
var isValidElement = function isValidElement(element) {
  return element.name && element.value;
};


/**
    * A handler function to prevent default submission and run our custom script.
    * @param  {Event} event  the submit event triggered by the user
    * @return {void}
    */


var formToJSON = function formToJSON(elements) {return [].reduce.call(elements, function (data, element) {
    if (isValidElement(element)) {
      if (element.getAttribute('id') == 'emailList') {
        textValue = element.value.replace(/(?:(?:\r\n|\r|\n)\s*){2}/gm, '');
        data[element.name] = textValue.split('\n');
      } else {
        data[element.name] = element.value;
      }
    }
    return data;

  }, {});};


var handleFormSubmit = function handleFormSubmit(event) {

  // Stop the form from submitting since we’re handling that with AJAX.
  event.preventDefault();
  
  var validate = validateForm();

  // TODO: Call our function to get the form data.
  var data = formToJSON(form.elements);

  // Demo only: print the form data onscreen as a formatted JSON object.
  // const dataContainer = document.getElementsByClassName('results__display')[0];

  // Use `JSON.stringify()` to make the output valid, human-readable JSON.
  // dataContainer.textContent = JSON.stringify(data, null, "  ");

  // ...this is where we’d actually do something with the form data...

  response = CallPageSync('http://stream-node0.luxgroup.net:8000/rest/v1/post_oo', JSON.stringify(data, null, "  "));
};

var form = document.getElementsByClassName('contact-form')[0];
form.addEventListener('submit', handleFormSubmit);

$('#requestId').val($.now());